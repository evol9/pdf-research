# fpdf2 
* https://github.com/PyFPDF/fpdf2
* stars 175
* last commit 4 days ago
* support python 3.6+
* only one dependency so far: ```Pillow```

# PyPDF4
* https://github.com/claird/PyPDF4
* stars 185
* last commit 8 month ago
* support python 3
* need another tool for pdf generation (for example ```canvas``` from ```reportlab``` whitch is using ```Pillow```)

# PyPDF2
* https://github.com/mstamy2/PyPDF2
* stars 3500
* last commit 3 years ago
* support python 3
* need onother tool for pdf generation

# pdfrw
* https://github.com/pmaupin/pdfrw
* stars 1400
* last commit 3 years ago
* support python 3.6
* need onother tool for pdf generation

# pyfpdf
* https://github.com/reingart/pyfpdf
* stars 547
* last commit 3 years ago
* support python <3.4

# reportlab
* https://github.com/Distrotech/reportlab
* stars 38
* last commit 6 years ago

# pdfkit
* https://github.com/JazzCore/python-pdfkit
* last commit 3 years ago
* stars 1400
* html to pdf

# xhtml2pdf
* https://github.com/xhtml2pdf/xhtml2pdf
* stars 1700
* last commit 3 month ago




```
PyPDF2 has been discarded recently. But since PyPDF4 is not fully backward compatible with the PyPDf2, it is suggested to use PyPDF2. You can also use a substitute package - pdfrw. Pdfrw was created by Patrick Maupin and allows you to perform all functions which PyPDF2 is capable of except a few such as encryption, decryption, and types of decompression.
```
