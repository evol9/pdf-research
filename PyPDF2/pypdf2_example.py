from io import BytesIO

from PyPDF2 import PdfFileWriter, PdfFileReader
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import letter, A4


def write_pdf():
	with BytesIO() as output:
		with BytesIO() as packet:
			can = Canvas(packet, pagesize=letter)
			can.drawString(10, 100, "Hello world")
			can.save()
			packet.seek(0)
			input_pdf = PdfFileReader(packet)
			page = input_pdf.getPage(0)

			output_pdf = PdfFileWriter()
			output_pdf.addPage(page)
			output_pdf.addBlankPage(width=216, height=280)
			output_pdf.write(output)

			output.seek(0)
			return output.read()

print(write_pdf())
