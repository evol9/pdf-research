from io import BytesIO
from contextlib import closing

from fpdf import FPDF


def write_pdf():
	with BytesIO() as output:
		with closing(FPDF(format='A3')) as pdf:
			pdf.add_page()
			pdf.set_font('helvetica', 'B', 16)
			pdf.cell(40, 10, 'Hello World!')
			pdf.output(output)

			output.seek(0)
			return output.read()


print(write_pdf())
